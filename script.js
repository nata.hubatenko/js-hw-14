window.onload = function () {
	const btn = document.querySelector('.btn');
	let topM = document.querySelector('.top-menu');

	let index = 0;
	const colors = ['cadetblue', '#35444F'];

	if (localStorage.getItem("color") !== null) {
		topM.style.backgroundColor = localStorage.getItem("color");
	}

	btn.addEventListener('click', function onClick() {
		topM.style.backgroundColor = colors[index];
		index = index >= colors.length - 1 ? 0 : index + 1;
		localStorage.setItem("color", topM.style.backgroundColor);
	});
}









